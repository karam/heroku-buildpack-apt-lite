# heroku-buildpack-apt-lite

*Note:* This is a fork of the official
[heroku-buildpack-apt](https://github.com/heroku/heroku-buildpack-apt) buildpack
that runs `apt-get install` with the `--no-install-recommends` option.

Add support for apt-based dependencies during both compile and runtime.

Added ability to also specify custom repositories through **:repo:** in `Aptfile` (see example below).

## Usage

This buildpack is not meant to be used on its own, and instead should be in used in combination with Heroku's [multiple buildpack support](https://devcenter.heroku.com/articles/using-multiple-buildpacks-for-an-app).

Include a list of apt package names to be installed in a file named `Aptfile`.

To find out what packages are available, see:
<https://packages.ubuntu.com>

See the [Heroku Stacks](https://devcenter.heroku.com/articles/stack) documention for which
Ubuntu LTS version is used by each Heroku stack.

## Example

#### Command-line

To use the edge version (i.e. the code in this repo):

```
heroku buildpacks:add --index 1 https://codeberg.org/karam/heroku-buildpack-apt-lite
```

#### Aptfile

    # you can list packages
    libexample-dev
    
    # or include links to specific .deb files
    https://downloads.example.com/example.deb
    
    # or add custom apt repos (only required if using packages outside of the standard Ubuntu APT repositories)
    :repo:deb https://apt.example.com/ example-distro main

## License

MIT
